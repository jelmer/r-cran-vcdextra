Explanation for binary files inside source package according to
  http://lists.debian.org/debian-devel/2013/09/msg00332.html

This package contains many example data sets

Files: data/Abortion.rda
Documentation: man/Abortion.Rd
     Example dataset:
     Opinions about abortion classified by gender and SES.

Files: data/Accident.RData
Documentation: man/Accident.Rd
     Data set used by Bertin (1983) to illustrate the cross-classification of
     data by numerous variables, each of which could have various types and
     could be assigned to various visual attributes.


Files: data/AirCrash.RData
Documentation: man/AirCrash.Rd
     Data on all fatal commercial airplane crashes from 1993-2015. Excludes
     small planes (less than 6 passengers) and non-commercial (cargo, military,
     private) aircraft.


Files: data/Alligator.RData
Documentation: man/Alligator.Rd
     Data from Agresti (2002) from a study of the
     primary food choices of alligators in four Florida lakes.
     Researchers classified the stomach contents of 219 captured
     alligators into five categories: Fish (the most common primary
     food choice), Invertebrate (snails, insects, crayfish, etc.),
     Reptile (turtles, alligators), Bird, and Other (amphibians,
     plants, household pets, stones, and other debris).


Files: data/Bartlett.rda
Documentation: man/Bartlett.Rd
     Data from an experiment to investigate the effect of cutting length (two
     levels) and planting time (two levels) on the survival of plum
     root cuttings, 240 cuttings were planted for each of the 2 x 2
     combinations of these factors, and their survival was later
     recorded. Bartlett (1935) used these data to illustrate a method for
     testing for no three-way interaction in a contingency table.


Files: data/Caesar.rda
Documentation: man/Caesar.Rd
     Data from infection from birth by Caesarian section, classified by
     ‘Risk’ (two levels), whether ‘Antibiotics’ were used (two levels)
     and whether the Caesarian section was ‘Planned’ or not.  The
     outcome is ‘Infection’ (three levels).


Files: data/Cancer.rda
Documentation: man/Cancer.Rd
     Example dataset:
     Three year survival of 474 breast cancer patients according to
     nuclear grade and diagnostic center.


Files: data/Cormorants.RData
Documentation: man/Cormorants.Rd
     Example dataset:
     Male double-crested cormorants use advertising behavior to attract
     females for breeding. In this study by Meagan Mc Rae (2015),
     cormorants were observed two or three times a week at six stations
     in a tree-nesting colony for an entire season, April 10, 2014-July
     10, 2014. The number of advertising birds was counted and these
     observations were classified by characteristics of the trees and
     nests.
     The goal is to determine how this behavior varies temporally over
     the season and spatially, as well as with characteristics of
     nesting sites.


Files: data/CyclingDeaths.RData
Documentation: man/CyclingDeaths.Rd
     Data containing the number of deaths of cyclists in London
     from 2005 through 2012 in each fortnightly period.  Aberdein &
     Spiegelhalter (2013) discuss these data in relation to the
     observation that six cyclists died in London between Nov. 5 and
     Nov. 13, 2013.


Files: data/DaytonSurvey.RData
Documentation: man/DaytonSurvey.Rd
     Data from Agresti (2002), Table 9.1, gives the result of a
     1992 survey in Dayton Ohio of 2276 high school seniors on whether
     they had ever used alcohol, cigarettes and marijuana.


Files: data/Depends.RData
Documentation: man/Depends.Rd
     Example dataset:
     This one-way table gives the type-token distribution of the number
     of dependencies declared in 4983 packages listed on CRAN on
     January 17, 2014.


Files: data/Detergent.rda
Documentation: man/Detergent.Rd
     Example dataset:
     Cross-classification of a sample of 1008 consumers according to
     (a) the softness of the laundry water used, (b) previous use of
     detergent Brand M, (c) the termperature of laundry water used and
     (d) expressed preference for Brand X or Brand M in a blind trial.


Files: data/Donner.RData
Documentation: man/Donner.Rd
     Data set containing information on the members of the Donner
     Party, a group of people who attempted to migrate to California in
     1846. They were trapped by an early blizzard on the eastern side
     of the Sierra Nevada mountains, and before they could be rescued,
     nearly half of the party had died.


Files: data/Draft1970.RData
Documentation: man/Draft1970.Rd
     Data set of the results of the 1970 US draft lottery.


Files: data/Draft1970table.RData
Documentation: man/Draft1970table.Rd
     Data set of the results of the 1970 US draft lottery, in
     the form of a frequency table. The rows are months of the year,
     Jan-Dec and columns give the number of days in that month which
     fall into each of three draft risk categories High, Medium, and
     Low, corresponding to the chances of being called to serve in the
     US army.


Files: data/Dyke.rda
Documentation: man/Dyke.Rd
     Observational data on a sample of 1729 individuals,
     cross-classified in a 2^5 table according to their sources of
     information (read newspapers, listen to the radio, do 'solid'
     reading, attend lectures) and whether they have good or poor
     knowledge regarding cancer.  Knowledge of cancer is often treated
     as the response.


Files: data/Fungicide.rda
Documentation: man/Fungicide.Rd
     Data from Gart (1971) on the carcinogenic effects of a certain
     fungicide in two strains of mice. Of interest is how the
     association between ‘group’ (Control, Treated) and ‘outcome’
     (Tumor, No Tumor) varies with ‘sex’ and ‘strain’ of the mice.
     Breslow (1976) used this data to illustrate the application of
     linear models to log odds ratios.


Files: data/Geissler.RData
Documentation: man/Geissler.Rd
     Data from Geissler (1889) on the distributions of boys and
     girls in families in Saxony, collected for the period 1876-1885.
     The ‘Geissler’ data tabulates the family composition of 991,958
     families by the number of boys and girls listed in the table
     supplied by Edwards (1958, Table 1).


Files: data/Gilby.rda
Documentation: man/Gilby.Rd
     Example dataset:
     Schoolboys were classified according to their clothing and to
     their teachers rating of "dullness" (lack of intelligence), in a 5
     x 7 table originally from Gilby (1911). Anscombe (1981) presents a
     slightly collapsed 4 x 6 table, used here, where the last two
     categories of clothing were pooled as were the first two
     categories of dullness due to small counts.
     Both ‘Dullnes’ and ‘Clothing’ are ordered categories, so models
     and methods that examine their association in terms of ordinal
     categories are profitable.


Files: data/GSS.rda
Documentation: man/GSS.Rd
     Data from the General Social Survey, 1991, on the relation between
     sex and party affiliation.


Files: data/HairEyePlace.RData
Documentation: man/HairEyePlace.Rd
     Example dataset:
     A three-way frequency table crossing eye color and hair color in
     two places, Caithness and Aberdeen, Scotland. These data were of
     interest to Fisher (1940) and others because there are mixtures of
     people of Nordic, Celtic and Anglo-Saxon origin. One or both
     tables have been widely analyzed in conjunction with RC and
     canonical correlation models for categorical data, e.g., Becker
     and Clogg (1989).


Files: data/Hauser79.RData
Documentation: man/Hauser79.Rd
     Example dataset:
     Hauser (1979) presented this two-way frequency table,
     cross-classifying occupational categories of sons and fathers in
     the United States.

Files: data/Heart.rda
Documentation: man/Heart.Rd
     Example dataset:
     Classification of individuals by gender, occupational category and
     occurrence of heart disease


Files: data/Heckman.rda
Documentation: man/Heckman.Rd
     Example dataset:
     1583 married women were surveyed over the years 1967-1971,
     recording whether or not they were employed in the labor force.
     The data, originally from Heckman & Willis (1977) provide an
     example of modeling longitudinal categorical data, e.g., with
     markov chain models for dependence over time.


Files: data/Hoyt.rda
Documentation: man/Hoyt.Rd
     Example dataset:
     Minnesota high school graduates of June 1930 were classified with
     respect to (a) ‘Rank’ by thirds in their graduating class, (b)
     post-high school ‘Status’ in April 1939 (4 levels), (c) ‘Sex’, (d)
     father's ‘Occupation’al status (7 levels, from 1=High to 7=Low).
     The data were first presented by Hoyt et al. (1959) and have been
     analyzed by Fienberg(1980), Plackett(1974) and others.


Files: data/ICU.RData
Documentation: man/ICU.Rd
     Example dataset:
     The ICU data set consists of a sample of 200 subjects who were
     part of a much larger study on survival of patients following
     admission to an adult intensive care unit (ICU), derived from
     Hosmer, Lemeshow and Sturdivant (2013) and Friendly (2000).


Files: data/JobSat.rda
Documentation: man/JobSat.Rd
     This data set is a contingency table of job satisfaction by income
     for a small sample of black males from the 1996 General Social
     Survey, as used by Agresti (2002) for an example.


Files: data/Mammograms.RData
Documentation: man/Mammograms.Rd
     Example dataset:
     Kundel & Polansky (2003) give (possibly contrived) data on a set
     of 110 mammograms rated by two readers.


Files: data/Mental.rda
Documentation: man/Mental.Rd
     Example dataset:
     A 6 x 4 contingency table representing the cross-classification of
     mental health status (‘mental’) of 1660 young New York residents
     by their parents' socioeconomic status (‘ses’).

Files: data/Mice.RData
Documentation: man/Mice.Rd
     Data from Kastenbaum and Lamphiear (1959). The table gives the
     number of depletions (deaths) in 657 litters of mice, classified
     by litter size and treatment.  This data set has become a classic
     in the analysis of contingency tables, yet unfortunately little
     information on the details of the experiment has been published.


Files: data/Mobility.rda
Documentation: man/Mobility.Rd
     Data on social mobility, recording the occupational category of
     fathers and their sons.


Files: data/PhdPubs.RData
Documentation: man/PhdPubs.Rd
     A data set giving the number of publications by doctoral
     candidates in biochemistry in relation to various predictors,
     originally from Long (1997).


Files: data/ShakeWords.RData
Documentation: man/ShakeWords.Rd
     This data set, from Efron and Thisted (1976), gives the number of
     distinct words types (‘Freq’) of words that appeared exactly once,
     twice, etc. up to 100 times (‘count’) in the complete works of
     Shakespeare.  In these works, Shakespeare used 31,534 distinct
     words (types), comprising 884,647 words in total.


Files: data/Titanicp.rda
Documentation: man/Titanicp.Rd
     Data on passengers on the RMS Titanic, excluding the Crew and some
     individual identifier variables.


Files: data/Toxaemia.RData
Documentation: man/Toxaemia.Rd
     Example dataset:
     Brown et al (1983) gave these data on two signs of toxaemia, an
     abnormal condition during pregnancy characterized by high blood
     pressure (hypertension) and high levels of protein in the urine.
     If untreated, both the mother and baby are at risk of
     complications or death.


Files: data/TV.rda
Documentation: man/TV.Rd
     Example dataset:
     This data set ‘TV’ comprises a 5 x 11 x 3 contingency table based
     on audience viewing data from Neilsen Media Research for the week
     starting November 6, 1995.


Files: data/Vietnam.rda
Documentation: man/Vietnam.Rd
     Example dataset:
     A survey of student opinion on the Vietnam War was taken at the
     University of North Carolina at Chapel Hill in May 1967 and
     published in the student newspaper. Students were asked to fill in
     ballot papers stating which policy out of A,B,C or D they
     supported. Responses were cross-classified by gender/year.


Files: data/Vietnam.RData
Documentation: man/Vietnam.Rd
     Example dataset:
     A survey of student opinion on the Vietnam War was taken at the
     University of North Carolina at Chapel Hill in May 1967 and
     published in the student newspaper. Students were asked to fill in
     ballot papers stating which policy out of A,B,C or D they
     supported. Responses were cross-classified by gender/year.


Files: data/Vote1980.RData
Documentation: man/Vote1980.Rd
     Data from the 1982 General Social Survey on votes in the 1980 U.S.
     presidential election in relation to race and political
     conservatism.


Files: data/WorkerSat.RData
Documentation: man/WorkerSat.Rd
     Example dataset:
     Blue collar workers job satisfaction from large scale
     investigation in Denmark in 1968 (Andersen, 1991).


Files: data/Yamaguchi87.RData
Documentation: man/Yamaguchi87.Rd
     Example dataset:
     Yamaguchi (1987) presented this three-way frequency table,
     cross-classifying occupational categories of sons and fathers in
     the United States, United Kingdom and Japan.  This data set has
     become a classic for models comparing two-way mobility tables
     across layers corresponding to countries, groups or time (e.g.,
     Goodman and Hout, 1998; Xie, 1992).


